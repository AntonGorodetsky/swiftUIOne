//
//  swiftUIOneApp.swift
//  swiftUIOne
//
//  Created by Anton Gorodetsky on 10.05.2022.
//

import SwiftUI

@main
struct swiftUIOneApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
